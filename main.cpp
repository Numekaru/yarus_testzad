#include <iostream>
#include <stdlib.h>
#include <getopt.h>
#include "author.h"
#include <tinyxml.h>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
const string MSG_USAGE =
    "Error! Usage: ./yarus_testzad --in <INPUT FILE> --out <OUTPUT FILE> --operation <OPERATION>\n";
int main(int argc, char **argv)
{
	int c;
	struct option args[] = {
		{"in", required_argument, 0, 'i'},
		{"out", required_argument, 0, 'o'},
		{"operation", required_argument, 0, 'p'},
		{0, 0, 0, 0}
	};
	string path_file_in, path_file_out;
	short int operation;
	int option_index = 0;
	bool checkI, checkO, checkP;
	checkI = checkO = checkP = 0;
	while ((c = getopt_long(argc, argv, "iop", args, &option_index)) != -1) {
		switch (c) {
		case 'i':
			path_file_in = optarg;
			checkI = 1;
			break;
		case 'o':
			path_file_out = optarg;
			checkO = 1;
			break;
		case 'p':
			operation = atoi(optarg);
			checkP = 1;
			break;
		default:
			cout << MSG_USAGE;
			return -1;
		}
	}
	if (checkI == 0 || checkP == 0 || checkO == 0) {
		cout << MSG_USAGE;
		return -1;
	};
	TiXmlDocument ti_in(path_file_in.c_str());
	if (!ti_in.LoadFile()) {
		cout << "Bad input file";
		return -1;
	}

	switch (operation) {
	case 1:
	case 2:
		break;
	default:
		cout << "Bad operation type";
		return -1;
	}
	TiXmlElement *catalog = 0;
	catalog = ti_in.FirstChildElement("Catalog");
	if (catalog == 0) {
		cout << "You dont have <Catalog>";
		return -1;
	}
	TiXmlElement *au = 0;
	vector < Author > r;
	au = catalog->FirstChildElement("Author");
	if (au == 0) {
		cout << "No authors met";
		return -1;
	}
	r.push_back(Author
		    (au->Attribute("name"), au->Attribute("surname"),
		     au->Attribute("birthday")));
	while ((au = au->NextSiblingElement("Author")) != 0) {
		r.push_back(Author
			    (au->Attribute("name"), au->Attribute("surname"),
			     au->Attribute("birthday")));
	}
	au = catalog->FirstChildElement("Author");
	int i = 0;
	au->SetAttribute("name", r[i].name);
	au->SetAttribute("surname", r[i].surname);
	au->SetAttribute("birthday", r[i].birthday);
	do {
		if (operation == 1) {
			std::reverse(r[i].name.begin(), r[i].name.end());
			std::reverse(r[i].surname.begin(), r[i].surname.end());
			std::reverse(r[i].birthday.begin(),
				     r[i].birthday.end());
			au->SetAttribute("name", r[i].name);
			au->SetAttribute("surname", r[i].surname);
			au->SetAttribute("birthday", r[i].birthday);
		} else {
			au->SetAttribute("name", r[i].name.length());
			au->SetAttribute("surname", r[i].surname.length());
			au->SetAttribute("birthday", r[i].birthday.length());
		}

		i++;
	} while ((au = au->NextSiblingElement("Author")) != 0);
	ti_in.SaveFile(path_file_out);
}
