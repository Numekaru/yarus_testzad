#include "author.h"
#include <iostream>
#include <algorithm>
using namespace std;
bool pred(char a)
{
	if (a > 0 && a < 256)
		return false;
	return true;
}

Author::Author(string n, string s, string b)
{
	name = n;
	surname = s;
	birthday = b;
	name.erase(std::remove_if(name.begin(), name.end(), pred), name.end());
	surname.erase(std::remove_if(surname.begin(), surname.end(), pred),
		      surname.end());
	birthday.erase(std::remove_if(birthday.begin(), birthday.end(), pred),
		       birthday.end());
}
