CC=g++
CFLAGS=-Wall -fpermissive -ltinyxml -c 
EXECUTABLE=yarus_testzad
TINYXML_USE_STL=YES
all:	main
main: main.o author.o 
	$(CC) -Wall  -o yarus_testzad  main.o author.o -ltinyxml
main.o: main.cpp
	$(CC) $(CFLAGS)  main.cpp -o main.o
author.o: author.cpp
	$(CC) $(CFLAGS) author.cpp -o author.o
clean:
	rm -rf *.o yarus_testzad
